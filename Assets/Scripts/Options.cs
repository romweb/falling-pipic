﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Options
{
    public string Version;
    public string SoundOn;
    public string MusicOn;
    public string Language;
    public string FirstStart;
}
