﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameGlobal;

public class ObjectsPool : MonoBehaviourEx
{
    [System.Serializable]
    public class TwoInt
    {
        public int min;
        public int max;
    }

    public Transform[] prefabs = null;
    public TwoInt[] prefabsChance = null;
    public Transform parentFolder = null;
    public Vector3[] panelPositions = null;
    public int maxObjectsInPoll = 50;
    public int bonusСhance = 30;

    public Transform lastObjectRef = null;
    public float generateDistance = 10f;

    private int panelPortalCount = 0;

	void Start ()
    {
        GM.ResetGameParams();

        if (GM.availableIndexs.Count == 0)
        {
            List<int> indexs = new List<int>();
            for (int i = 0; i < maxObjectsInPoll; i++)
                indexs.Add(CreateObject(false));
            GM.availableIndexs = indexs;
        }

        StartCoroutine(ObjectGenerator());
	}

    void OnDestroy()
    {
        StopCoroutine(ObjectGenerator());
    }

    int GetPrefabIndex()
    {
        int res = -1;
        float rnd = UnityEngine.Random.Range(1, 100);
        for (int i = 0; i < prefabsChance.Length; i++)
        {
            if (rnd >= prefabsChance[i].min && rnd <= prefabsChance[i].max)
            {
                res = i;
                break;
            }
        }

        return res;
    }

    int GetPanelIndex()
    {
        int index = -1;
        int tmp = -1;

        try
        {
            tmp = UnityEngine.Random.Range(0, GM.availableIndexs.Count);
            index = GM.availableIndexs[tmp];

            if (GM.panels[index].gameObject.name == "PanelWithPortal(Clone)")
            {
                if (panelPortalCount <= GM.panelCountBetweenPortals)
                {
                    for (int i = 0; i < GM.availableIndexs.Count; i++)
                    {
                        int indexTmp = GM.availableIndexs[i];
                        if (GM.panels[indexTmp].gameObject.name == "PanelNormal(Clone)")
                        {
                            tmp = i;
                            index = indexTmp;
                            break;
                        }
                    }
                }
                else
                    panelPortalCount = 0;
            }

            panelPortalCount++;
            GM.availableIndexs.RemoveAt(tmp);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        return index;
    }

    int CreateObject(bool active = true)
    {
        int index = -1;
        int posIndex = -1;
        int prefabIndex = -1;

        try
        {
            posIndex = Mathf.RoundToInt(UnityEngine.Random.Range(0, panelPositions.Length*100)/100);

            if (GM.availableIndexs.Count > 0 &&
                GM.panels.Count >= maxObjectsInPoll)
            {
                index = GetPanelIndex();
            }
            else
            {
                prefabIndex = GetPrefabIndex();
                if (prefabIndex != -1)
                {
                    Transform tmp = Instantiate(
                                prefabs[prefabIndex],
                                panelPositions[posIndex],
                                Quaternion.identity) as Transform;
                    if (tmp != null)
                    {
                        PanelController script = tmp.gameObject.GetComponent<PanelController>();
                        if (script != null)
                        {
                            index = script.id = GM.panels.Count;
                            GM.panels.Add(tmp);
                        }
                    }
                }
            }

            if (index != -1)
            {
                GM.panels[index].SetParent(parentFolder);

                if (active)
                {
                    GM.panels[index].position = new Vector2(panelPositions[posIndex].x, lastObjectRef.position.y - generateDistance);
                    lastObjectRef = GM.panels[index].transform;

                    PanelController script = GM.panels[index].gameObject.GetComponent<PanelController>();
                    if (script != null)
                        script.Reset();
                }

                GM.panels[index].gameObject.SetActive(active);
            }
        }
        catch (ArgumentOutOfRangeException e)
        {
            Debug.Log("objectGenerator IndexOutOfRangeException, index=" + posIndex + ", prefabIndex=" + prefabIndex + ". " + e.Source);
        }

        return index;
    }

    IEnumerator ObjectGenerator()
    {
        while (true)
        {
            yield return new WaitForSeconds(GM.generateObjectsPeriod);

            CreateObject();
        }
    }
}
