﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using TouchScript.Hit;
using TouchScript.Gestures.Simple;
using GameGlobal;

[RequireComponent(typeof(MetaGesture))]
public class PanelController : MonoBehaviourEx   {

    [HideInInspector]
    public int id = -1;

    public float stopInPanelTime = 1.0f;
    public Sprite panelWithBonus = null;
    public Sprite panelWithoutBonus = null;

    public AudioClip soundPortalRef = null;

    [HideInInspector]
    public Transform bonusRef = null;
    [HideInInspector]
    private BonusData bonusData = null;
    public bool canBonus = false;
    [HideInInspector]
    public int bonusIndex = -1;
    [HideInInspector]
    public bool isGorillaFacingRight = false;

    bool enableMoving = true;
    bool toched = false;
    Vector3 targetPos = Vector3.zero;
    Transform heroRef = null;

    public Transform haloRef = null;

    int prevPanelId = 0;

    public AudioClip soundPanelRef = null;
    public AudioClip soundMetalPanelRef = null;

    private CameraController cameraScriptRef = null;

    private float changeAlphaValue = 1f;

    private bool activePanel = true;

    public bool IsActivePanel()
    {
        return activePanel;
    }

    private void OnEnable()
    {
        GetComponent<MetaGesture>().TouchBegan += TouchBeganHandler;
        GetComponent<MetaGesture>().TouchMoved += TouchMovedHandler;
        GetComponent<MetaGesture>().TouchEnded += TouchEndedHandler;
        GetComponent<MetaGesture>().TouchCancelled += TouchCancelledHandler;
    }

    private void OnDisable()
    {
        GetComponent<MetaGesture>().TouchBegan -= TouchBeganHandler;
        GetComponent<MetaGesture>().TouchMoved -= TouchMovedHandler;
        GetComponent<MetaGesture>().TouchEnded -= TouchEndedHandler;
        GetComponent<MetaGesture>().TouchCancelled -= TouchCancelledHandler;
    }

    bool IsGorillaBonus()
    {
        if (bonusRef)
            return bonusRef.gameObject.name == "BonusGorilla(Clone)";

        return false;
    }

    public void GetPosition()
    {
        if (targetPos.x < GM.leftRegionPosition)
            targetPos.x = GM.leftRegionPosition;
        else if (targetPos.x > GM.rightRegionPosition)
            targetPos.x = GM.rightRegionPosition;

        if (bonusRef && IsGorillaBonus())
        {
            if (!isGorillaFacingRight && myTransform.position.x < 0)
                GorillaFlipSprite(bonusRef);
            else if (isGorillaFacingRight && myTransform.position.x > 0)
                GorillaFlipSprite(bonusRef);
        }
    }

	void Awake ()
    {
        _Awake();
        Reset();
    }

	void Start ()
    {
        gameObject.SetActive(true);

        heroRef = GM.heroTransformRef;
        cameraScriptRef = thisCamera.GetComponent<CameraController>();

        if (haloRef)
            haloRef.renderer.enabled = false;
	}

    public void Reset()
    {
        activePanel = true;
        targetPos = myTransform.position;
        enableMoving = true;
        toched = false;
        EnableAllTriggers(gameObject, false);
        isGorillaFacingRight = false;
        prevPanelId = 0;

        changeAlphaValue = 1f;
        ChangeAlpha();

        if (bonusRef != null)
        {
            DestroyObject(bonusRef.gameObject);
            bonusRef = null;
        }

        if (panelWithoutBonus)
            mySpriteRenderer.sprite = panelWithoutBonus;
    }

    void FixedUpdate()
    {
        if (enableMoving)
            myRigidbody2D.MovePosition(targetPos);
    }
	
	void Update () 
    {
        if (enableMoving)
            GetPosition();
	}

    int AddExtraBonus()
    {
        int res = -1;

        if (!canBonus || bonusRef != null)
            return bonusIndex;

        try
        {
            int needBonus = UnityEngine.Random.Range(0, 100);
            if (needBonus > 40)
            {
                if (GM.needAddBonusClockNow && needBonus < 70)
                {
                    if (GM.HaveClockBonus())
                    {
                        GM.needAddBonusClockNow = false;
                    }
                    else if (GM.extraBonusClockStartTime == 0.0f || (Time.timeSinceLevelLoad - GM.extraBonusClockStartTime) > GM.withoutExtraBonusClockInterval)
                    {
                        //Debug.Log("Extra bonuc clock! " + GM.extraBonusClockStartTime + " / " + (Time.timeSinceLevelLoad - GM.extraBonusClockStartTime) + " / " + GM.withoutExtraBonusClockInterval);

                        GetBonus(GM.clockBonusIndex);
                        GM.needAddBonusClockNow = false;
                        GM.extraBonusClockStartTime = Time.timeSinceLevelLoad;
                        res = GM.clockBonusIndex;
                    }
                }
                else if (GM.HaveMetalBonus())
                {
                    if (GM.extraBonusTrapStartTime == 0.0f || (Time.timeSinceLevelLoad - GM.extraBonusTrapStartTime) > GM.withoutExtraBonusTrapInterval)
                    {
                        //Debug.Log("Extra bonuc trap! " + GM.extraBonusTrapStartTime + " / " + (Time.timeSinceLevelLoad - GM.extraBonusTrapStartTime) + " / " + GM.withoutExtraBonusTrapInterval);

                        GetBonus(GM.trapBonusIndex);
                        GM.extraBonusTrapStartTime = Time.timeSinceLevelLoad;
                        res = GM.trapBonusIndex;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        return res;
    }

    int AddRandomBonus()
    {
        int resIndex = -1;

        if (!canBonus || bonusRef != null)
            return bonusIndex;

        try
        {
            bool needBonus = false;
            if (GM.panelCount >=  GM.panelCountBetweenBonus + 2)
            {
                needBonus = (UnityEngine.Random.Range(0, 100) > 50);
                if (needBonus)
                    GM.panelCount = 0;
            }

            GM.panelCount++;

            if (needBonus)
            {
                do
                {
                    resIndex = Mathf.RoundToInt(UnityEngine.Random.Range(0, GM.bonus.Length*100) / 100);
                    if (resIndex == GM.lastBonusIndex)
                        resIndex = -1;
                    else if (resIndex == GM.clockBonusIndex && (GM.GetLevelTime(true) < GM.periodWithoutBonusClock))
                        resIndex = -1;

                } while (resIndex == -1);

                GetBonus(resIndex);
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }

        return resIndex;
    }

    bool GetBonus(int index)
    {
        bool res = false;

        try
        {
            if (index != -1)
            {
                bonusRef = Instantiate(GM.bonus[index]) as Transform;
                if (bonusRef != null)
                {
                    bonusRef.SetParent(myTransform);
                    bonusRef.localScale = GM.bonusLocalScale[index];
                    bonusRef.localPosition = GM.bonusLocalPositions[index];

                    bonusData = bonusRef.GetComponent<BonusData>();

                    if (bonusRef.gameObject.name == "BonusGorilla(Clone)" && myTransform.position.x < 0f)
                        GorillaFlipSprite(bonusRef);

                    bonusRef.gameObject.SetActive(true);

                    if (panelWithBonus)
                        mySpriteRenderer.sprite = panelWithBonus;

                    GM.lastBonusIndex = bonusIndex = index;
                    res = true;
                }
            }
        }
        catch(Exception e)
        {
            Debug.LogException(e);
        }

        return res;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        switch(coll.gameObject.name)
        {
            case "GroundCheck":
                if (!IsGorillaBonus())
                    Invoke("JumpNow", GM.HaveClockBonus() ? stopInPanelTime * GM.stopInPanelClockFactor : stopInPanelTime * (1f + GM.verticalSpeed/10f));
                break;
            case "PanelsRegion":
                EnableAllTriggers(gameObject, true);
                break;
            case "BonusRegion":
                AddRandomBonus();
                break;
            case "ExtraBonusRegion":
                AddExtraBonus();
                break;
            case "TopSpeedZoneRegion":
                JumpNow();
                break;
            case "AddPoolRegion":
                if (id != -1)
                    GM.availableIndexs.Add(id);
                enableMoving = false;
                gameObject.SetActive(false);
                break;
        }
    }

    void Invoke_DisableHalo()
    {
        if (haloRef)
            haloRef.renderer.enabled = false;
    }

    public void EnableAllTriggers(GameObject obj, bool value)
    {
        try
        {
            Collider2D[] colliders = obj.GetComponents<Collider2D>();
            for (int i = 1; i < colliders.Length; i++)
                colliders[i].isTrigger = value;

            Collider2D[] childColliders = obj.GetComponentsInChildren<Collider2D>();
            for (int i = 1; i < childColliders.Length; i++)
                childColliders[i].isTrigger = value;

            if (haloRef)
                haloRef.renderer.enabled = false;
        }
        catch (ArgumentOutOfRangeException e)
        {
            Debug.LogException(e);
        }
    }

    void PlaySoundEffect(AudioClip clip)
    {
        if (GM.options.SoundOn == "Yes" && IsActivePanel())
            myAudio.PlayOneShot(clip);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.name == "Hero")
        {
            if (haloRef && IsActivePanel())
            {
                haloRef.renderer.enabled = true;
                Invoke("Invoke_DisableHalo", 0.4f);
            }

            float factor = (GM.HaveClockBonus() ? 1 : 1);

            switch (GM.hezoZone)
            {
                case HeroZone.SpeepZone1:
                    targetPos.y += GM.verticalPositionFactorSpeedZone1 * factor;
                    if (cameraScriptRef)
                        cameraScriptRef.ChangeVerticalTarget(GM.verticalCameraFactorSpeedZone1 * factor);
                    break;
                case HeroZone.SpeepZone2:
                    targetPos.y += GM.verticalPositionFactorSpeedZone2 * factor;
                    if (cameraScriptRef)
                        cameraScriptRef.ChangeVerticalTarget(GM.verticalCameraFactorSpeedZone2 * factor);
                    break;
                case HeroZone.NormalZone:
                    if (cameraScriptRef)
                        cameraScriptRef.ChangeVerticalTarget(GM.verticalCameraFactorNormalZone);
                    break;
            }

            GM.hezoZone = HeroZone.NormalZone;
            PlaySoundEffect((GM.metalBonusCount > 0) ? soundMetalPanelRef : soundPanelRef);
        }
    }

    void TouchBeganHandler(object sender, EventArgs e)
    {
        toched = true;
    }

    void TouchMovedHandler(object sender, EventArgs e)
    {
        if (toched)
        {
            Vector3 pos = thisCamera.ScreenToWorldPoint(Input.mousePosition);
            targetPos.x = pos.x;
        }
    }

    void TouchEndedHandler(object sender, EventArgs e)
    {
        toched = false;
    }

    void TouchCancelledHandler(object sender, EventArgs e)
    {
        toched = false;
    }

    public void GorillaFlipSprite(Transform trans)
    {
        Vector3 theScale = trans.localScale;
        theScale.x *= -1;
        trans.localScale = theScale;
        trans.localPosition = new Vector3(trans.localPosition.x * -1f, trans.localPosition.y, trans.localPosition.z);
        isGorillaFacingRight = !isGorillaFacingRight;
    }

    public void JumpNow()
    {
        if (prevPanelId != this.gameObject.GetInstanceID())
        {
            prevPanelId = this.gameObject.GetInstanceID();

            EnableAllTriggers(this.gameObject, true);

            float x = 0f;
            if (myTransform.position.x <= 3.0f && myTransform.position.x >= -3.0f)
            {
                float tmp = UnityEngine.Random.Range(-100f, 100f);
                x = (tmp >= 0) ? -GM.stopInPanelVelocityFactor : GM.stopInPanelVelocityFactor;
            }
            else if (myTransform.position.x < -3.0f)
                x = GM.stopInPanelVelocityFactor;
            else if (myTransform.position.x > 3.0f)
                x = -GM.stopInPanelVelocityFactor;

            heroRef.gameObject.rigidbody2D.velocity = new Vector2(
                heroRef.gameObject.rigidbody2D.velocity.x + x * UnityEngine.Random.Range(1f, 1.5f),
                heroRef.gameObject.rigidbody2D.velocity.y - GM.stopInPanelVelocityFactor * UnityEngine.Random.Range(1f, 3f));

            heroRef.gameObject.rigidbody2D.AddForce(heroRef.gameObject.rigidbody2D.velocity);

            if (gameObject.name == "PanelWithPortal(Clone)")
            {
                if (soundPortalRef)
                    PlaySoundEffect(soundPortalRef);
            }

            SetInactivePanel();
        }
    }

    public void SetInactivePanel()
    {
        if (!GM.IsKilled())
        {
            changeAlphaValue = GM.hidenAlpha;
            Invoke("ChangeAlpha", GM.hidenAlphaTime);
            activePanel = false;
            if (bonusData)
            {
                bonusData.SetBonusActive(false);
                bonusData.gameObject.collider2D.isTrigger = true;
            }
        }
    }

    void ChangeAlpha()
    {
        Color newColor = mySpriteRenderer.color;
        newColor.a = changeAlphaValue;
        mySpriteRenderer.color = newColor;
        if (bonusData)
            bonusData.ChangeAlpha(changeAlphaValue);
    }
}
