﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class BonusTrap : MonoBehaviourEx
{
    private bool isMetalBonus = false;

    void AnimationEndedMetal()
    {
        myAnimator.SetBool("NeedClick", false);
        myAnimator.SetBool("NeedClickMetal", false);

        if (GM.HaveMetalBonus())
            GM.AddScore(GM.scoreTrap);
    }

    void MetalBonusFixed()
    {
        isMetalBonus = GM.HaveMetalBonus();
    }

    void AnimationEnded()
    {
        myAnimator.SetBool("NeedClick", false);
        myAnimator.SetBool("NeedClickMetal", false);

        if (!isMetalBonus)
            GM.KillHero();
    }
}
