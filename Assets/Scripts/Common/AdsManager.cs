﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour
{
    [HideInInspector]
    public static AdsManager instance;    // Patron Singleton

    public bool useBottomBanner = false;
    public bool useInterstitialBanner = false;

    public string androidAdsBannerId = null;
    public string androidAdsInterstitialBannerId = null;
    
    public bool useTestDevice = false;
    public string androidTestDevice = null;

    private BannerView bannerView = null;
    private InterstitialAd interstitial;
    private bool interstitialLoaded = false;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        if (useBottomBanner)
        {
            bannerView = new BannerView(androidAdsBannerId, AdSize.SmartBanner, AdPosition.Bottom);

            AdRequest request = null;
            if (useTestDevice)
                request = new AdRequest.Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)  
                    .AddTestDevice(androidTestDevice)
                    .Build();
            else
                request = new AdRequest.Builder().Build();
            if (request != null)
                bannerView.LoadAd(request);
        }

        if (useInterstitialBanner)
        {
            interstitial = new InterstitialAd(androidAdsInterstitialBannerId);
            if (interstitial != null)
            {
                interstitial.AdClosed += HandleInterstitialClosed;
                interstitial.AdLoaded += HandleInterstitialLoaded;

                AdRequest request = null;
                if (useTestDevice)
                    request = new AdRequest.Builder()
                        .AddTestDevice(AdRequest.TestDeviceSimulator)  
                        .AddTestDevice(androidTestDevice).Build();
                else
                    request = new AdRequest.Builder().Build();
                if (request != null)
                    bannerView.LoadAd(request);
                if (request != null)
                    interstitial.LoadAd(request);
            }
        }
    }

    public void ShowBanner(bool show = true)
    {
        if (bannerView != null)
        {
            if (show)
                bannerView.Show();
            else
                bannerView.Hide();
        }
    }

    public void ShowInterstitial()
    {
        ShowBanner(false);

        if (interstitial != null)
        {
            if (interstitial.IsLoaded())
                interstitial.Show();
        }
    }

    public bool IsInterstitialLoaded()
    {
        return (interstitial != null) && interstitialLoaded;
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        Application.Quit();
    }

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        interstitialLoaded = true;
    }

    void OnDestroy()
    {
        if (interstitial != null)
            interstitial.Destroy();
        if (bannerView != null)
            bannerView.Destroy();
    }
}