﻿using UnityEngine;
using System;
using System.Collections;
using GameGlobal;

public class Twitter : MonoBehaviourEx {

    const string Address = "http://twitter.com/intent/tweet";

    /// <summary>
    /// Адрес, который будет добавлен в твит
    /// </summary>
    public string url = null;
    /// <summary>
    /// Аккаунт, на который будет предложено подписаться после отправки твита, если пользователь не подписан на них
    /// </summary>
    public string related = null;

    public void Share()
    {
        string tmp = LanguageManager.GetText("social");

        Application.OpenURL(Address +
            "?text=" + WWW.EscapeURL(String.Format(tmp, GM.records.MaxRecord)) +
            "&url=" + WWW.EscapeURL(url) +
            "&related=" + WWW.EscapeURL(related) +
            "&lang=" + WWW.EscapeURL(LanguageManager.GetText("Langcode")));
    }
}
