﻿using UnityEngine;
using System.Collections;
using GameGlobal;

/// <summary>
/// Назначение коллайдеров кадрам (спрайтам) анимации
/// </summary>
public class SpriteCollider : MonoBehaviourEx
{
    [SerializeField]
    private PolygonCollider2D[] colliders = null;
    private int currentColliderIndex = 0;

    void Start()
    {
        if (colliders == null)
            Debug.LogError("SpriteCollider.colliders isn't defined!");
    }
    /// <summary>
    /// Назначает коллайдер указанному кадру анимации
    /// </summary>
    /// <param name="spriteNum">Номер кадра анимации (спрайта)</param>
    public void SetColliderForSprite(int spriteNum)
    {
        colliders[currentColliderIndex].enabled = false;
        currentColliderIndex = spriteNum;
        colliders[currentColliderIndex].enabled = true;
    }
}