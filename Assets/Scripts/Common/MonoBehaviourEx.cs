﻿using UnityEngine;
using System.Collections;
using GameGlobal;

/// <summary>
/// Используется для кэширования объектов типа Transform
/// </summary>
public class MonoBehaviourEx : MonoBehaviour
{
    public void _Awake()
    {
        myTransform = transform;
        myAnimator = gameObject.GetComponent<Animator>();
        mySpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        myParticleSystem = particleSystem;
        myRigidbody2D = rigidbody2D;
        myRenderer = renderer;
        myAudio = audio;
        thisCamera = Camera.main;
    }

    void Awake()
    {
        _Awake();
    }

    [HideInInspector]
    public Transform myTransform;
    [HideInInspector]
    public Animator myAnimator;
    [HideInInspector]
    public ParticleSystem myParticleSystem;
    [HideInInspector]
    public Rigidbody2D myRigidbody2D;
    [HideInInspector]
    public Renderer myRenderer;
    [HideInInspector]
    public AudioSource myAudio;
    [HideInInspector]
    public Camera thisCamera;
    [HideInInspector]
    public SpriteRenderer mySpriteRenderer;

    [HideInInspector]
    public static GameManager GM
    {
        get
        {
            if (GameManager.instance == null)
            {
                GameManager.instance = new GameManager();
                GameManager.instance.Init();
            }

            return GameManager.instance;
        }
    }

    [HideInInspector]
    public static AdsManager AM
    {
        get
        {
            if (AdsManager.instance == null)
            {
                AdsManager.instance = new AdsManager();
            }

            return AdsManager.instance;
        }
    }
}