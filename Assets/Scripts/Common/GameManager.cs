﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JsonFx.Json;
using JsonFx.Serialization;
using JsonFx.IO;
using UnityEngine.UI;

namespace GameGlobal
{
    public enum HeroZone { NormalZone, SpeepZone1, SpeepZone2, KillZone, TopZone };
    public enum BonusTextType { Score, ClockBonus, MetalBonus };

    public class GameManager : MonoBehaviour
    {
        const string gameVersion = "1.0.11";
        public static GameManager instance = null;

        private bool levelHelpStarted = false;

        [HideInInspector]
        public bool needLevelHelp = false;
        [HideInInspector]
        public Transform heroTransformRef = null; // Инициализируется в HeroController!
        [HideInInspector]
        public Animator helpRef = null;
        [HideInInspector]
        public Transform scoreLabelRef = null; // Инициализируется в ScoreText!
        [HideInInspector]
        public Transform metalBonusLabelRef = null; // Инициализируется в metalBonusText!
        [HideInInspector]
        public Transform clockBonusLabelRef = null; // Инициализируется в clockBonusText!
        [HideInInspector]
        public BonusText bonusLevelTextRef = null; // Инициализируется в BonusText!
        [HideInInspector]
        public ScoreTextLevel scoreLevelTextRef = null; // Инициализируется в ScoreTextLevel!
        [HideInInspector]
        public float verticalSpeed = 0.03f;
        [HideInInspector]
        public int metalBonusCount = 0;
        [HideInInspector]
        public int clockBonusCount = 0;
        [HideInInspector]
        public List<Transform> panels = new List<Transform>();
        [HideInInspector]
        public List<int> availableIndexs = new List<int>();
        [HideInInspector]
        public float pauseTimeScale = 0;
        [HideInInspector]
        public bool gamePaused = false;
        [HideInInspector]
        public Options options;
        [HideInInspector]
        public Records records;
        [HideInInspector]
        float verticalSpeedDefault = .0f;
        [HideInInspector]
        public int score = 0;
        [HideInInspector]
        public float prevDistanceForScore = 0f;
        [HideInInspector]
        public float generateObjectsPeriod = 0f;
        [HideInInspector]
        public HeroZone hezoZone = HeroZone.NormalZone;
        [HideInInspector]
        public int panelCount = 0;
        [HideInInspector]
        public int lastBonusIndex = -1;
        [HideInInspector]
        public float startGameTime = 0;
        [HideInInspector]
        public float endGameTime = 0;
        [HideInInspector]
        public float pauseGameTime = 0;
        [HideInInspector]
        public bool needAddBonusClockNow = false;
        [HideInInspector]
        public float extraBonusClockStartTime = 0f;
        [HideInInspector]
        public float extraBonusTrapStartTime = 0f;
        [HideInInspector]
        public float verticalSpeedSave = 0f;

        bool heroKilled = false;
        bool highScoreBigShown = false;

        [Header("Настройка игры")]

        [Tooltip("Стартовая скорость")]
        public float startVerticalSpeed = 2f;
        [Tooltip("Период повторения генерации объектов из пула")]
        public float repeatTimeGenerateObject = 3f;
        [Tooltip("Период измениея скорости")]
        public float repeatTimeChangeSpeed = 3f;
        [Tooltip("Коэффицент для расчёта изменения скорости")]
        public float verticalSpeedFactor = 10f;
        [Tooltip("Максимальная вертикальная скорость")]
        public float maxVerticalSpeed = 40f;
        [Tooltip("Ссылка на фоновый свук в меню")]
        public AudioSource soundMenuRef = null;
        [Tooltip("Ссылка на фоновый свук в игре")]
        public AudioSource soundLevelRef = null;
        [Tooltip("Звук конца игры")]
        public AudioClip soundEndRef = null;
        [Tooltip("Звук для обучения")]
        public AudioClip soundHelpRef = null;
        [Tooltip("Показывать рекламу")]
        public bool showAds = true;
        
        [Header("Настройка бонусов и очков")]

        [Tooltip("Дистанция, которую надо пройти для начисления очков")]
        public float distanceForScore = 10f;
        [Tooltip("Очки за прохождение дистанции")]
        public int scoreDistance = 100;
        [Tooltip("Очки за уничтожение капкана или гориллы")]
        public int scoreTrap = 200;
        [Tooltip("Период действия бонуса Металлический Шар")]
        public float metalBallInterval = 5f;
        [Tooltip("Период действия бонуса Часы")]
        public float clockInterval = 5f;
        [Tooltip("Скорость изменения игры в режиме бонуса Часы")]
        public float verticalSpeedPause = 1f;
        [Tooltip("Коэффицент для задержки шара на панелях в режиме бонуса Часы")]
        public float stopInPanelClockFactor = 3f;
        [Tooltip("Минимальное количество панелей между порталами")]
        public int panelCountBetweenPortals = 5;
        [Tooltip("Минимальное количество панелей для вывода бонуса")]
        public int panelCountBetweenBonus = 5;
        [Tooltip("Звук начисления очков")]
        public AudioClip soundScoreRef = null;
        [Tooltip("Игровые бонусы")]
        public Transform[] bonus = null;
        [Tooltip("Индекс бонуса Часы из Transform[] bonus")]
        public int clockBonusIndex = -1;
        [Tooltip("Индекс бонуса Капкан из Transform[] bonus")]
        public int trapBonusIndex = -1;
        [Tooltip("Индекс бонуса Горилла из Transform[] bonus")]
        public int gorillaBonusIndex = -1;
        [Tooltip("Позиция относительно панели")]
        public Vector2[] bonusLocalPositions = null;
        [Tooltip("Размеры бонусов")]
        public Vector2[] bonusLocalScale = null;
        [Tooltip("Период в течении которого не добавляем бонус часы")]
        public float periodWithoutBonusClock = 2f;
        [Tooltip("Вертикальная минимальная скорость когда надо чаще добавлять бонус часы")]
        public float speedBonusClock = 4f;
        [Tooltip("Интервал времении, в течении которого не добавляется extra bonus clock")]
        public float withoutExtraBonusClockInterval = 4f;
        [Tooltip("Интервал времении, в течении которого не добавляется extra bonus trap")]
        public float withoutExtraBonusTrapInterval = 4f;     
        
        [Header("Настройка шара")]

        [Tooltip("Коэффицент сдвига шара при его соскальзовании с панели")]
        public float stopInPanelVelocityFactor = 0.4f;
        [Tooltip("Позиция по горизонтали возникновения шара после прохождение его через вертикальные границы экрана")]
        public float recreateHeroPosX = 0f;
        [Tooltip("Ускорение по горизонтали при возникновении шара после прохождение его через вертикальные границы экрана")]
        public Vector2 recreateHeroForce = Vector2.zero;
        [Tooltip("Сопротивление падению нормального шара (чем меньше тем быстрее падает)")]
        public float normalVerticalDrag = 0.8f;
        [Tooltip("Сопротивление падению металлического шара (чем меньше тем быстрее падает)")]
        public float metalVerticalDrag = 0.6f;
        [Tooltip("Дополнительное cопротивление падению шара в SpeedZone1")]
        public float verticalDragFactorSpeedZone1 = 0.2f;
        [Tooltip("Дополнительное cопротивление падению шара в SpeedZone2")]
        public float verticalDragFactorSpeedZone2 = 0.4f;
        [Tooltip("Дополнительное cопротивление падению шара в TopZone")]
        public float verticalDragFactorTopZone = 0f;
        [Tooltip("Нормальное Gravity Scale")]
        public float normalGravityScale = 1f;
        [Tooltip("Gravity Scale в TopZone")]
        public float topZoneGravityScale = 5f;

        [Header("Настройка панелей")]

        [Tooltip("Позиция левой вертикальной границы для перемещения панелей")]
        public float leftRegionPosition = -4.3f;
        [Tooltip("Позиция правой вертикальной границы для перемещения панелей")]
        public float rightRegionPosition = 4.3f;
        [Tooltip("Дополнительное подпрыгивание панели вверх при падению шара в SpeepZone1")]
        public float verticalPositionFactorSpeedZone1 = 0.1f;
        [Tooltip("Дополнительное подпрыгивание панели вверх при падению шара в SpeepZone2")]
        public float verticalPositionFactorSpeedZone2 = 0.3f;
        [Tooltip("Прозрачность для панелей, на которых побывал шар")]
        public float hidenAlpha = 0.4f;
        [Tooltip("Задержка перед прозрачностью для панелей, на которых побывал шар")]
        public float hidenAlphaTime = 1f;

        [Header("Настройка камеры")]

        [Tooltip("Дополнительное смещение камеры вниз при падении шара в NormalZone")]
        public float verticalCameraFactorNormalZone = -0.2f;
        [Tooltip("Дополнительное смещение камеры вниз при падении шара в SpeepZone1")]
        public float verticalCameraFactorSpeedZone1 = -1f;
        [Tooltip("Дополнительное смещение камеры вниз при падении шара в SpeepZone2")]
        public float verticalCameraFactorSpeedZone2 = -2.2f;

        public void AddScore(int scoreValue)
        {
            if (!IsKilled())
            {
                score += scoreValue;
                if (scoreLevelTextRef)
                {
                    if (score > records.MaxRecord && !highScoreBigShown)
                    {
                        highScoreBigShown = true;
                        scoreLevelTextRef.ShowText(BonusTextType.Score, "HighscoreBig", scoreValue);
                    }
                    else
                        scoreLevelTextRef.ShowText(BonusTextType.Score, "ScoreText", scoreValue);
                }
            }
        }

        public void ScoreUpdate()
        {
            if (scoreLabelRef)
            {
                Text text = scoreLabelRef.GetComponent<Text>();
                text.text = score.ToString();
                if (soundScoreRef)
                    PlaySoundEffect(soundScoreRef);
            }
        }

        public void ResetGameParams()
        {
            pauseGameTime = endGameTime = startGameTime = 0f;
            verticalSpeed = verticalSpeedDefault;
            metalBonusCount = 0;
            clockBonusCount = 0;
            levelHelpStarted = false;
            panelCount = 0;
            GameManager.instance.verticalSpeed = startVerticalSpeed;
            score = 0;
            prevDistanceForScore = 0f;
            lastBonusIndex = -1;
            needAddBonusClockNow = false;
            extraBonusClockStartTime = extraBonusTrapStartTime = 0f;
            verticalSpeedSave = 0f;

            if (panels.Count != 0)
                panels.RemoveRange(0, panels.Count);
            if (availableIndexs.Count != 0)
                availableIndexs.RemoveRange(0, availableIndexs.Count);

            heroKilled = gamePaused = false;

            generateObjectsPeriod = GameManager.instance.repeatTimeGenerateObject;
        }

        public void AddClockBonus()
        {
            if (!IsKilled())
            {
                if (bonusLevelTextRef)
                    bonusLevelTextRef.ShowText(BonusTextType.ClockBonus, "DelayBonusText", 1);
            }
        }

        public void DeleteClockBonus()
        {
            clockBonusCount--;
            if (clockBonusCount < 0)
                clockBonusCount = 0;
            Text text = clockBonusLabelRef.GetComponent<Text>();
            text.text = clockBonusCount.ToString();
        }

        public void AddMetalBonus()
        {
            if (!IsKilled())
            {
                if (bonusLevelTextRef)
                    bonusLevelTextRef.ShowText(BonusTextType.MetalBonus, "MetalBonusText", 1);
            }
        }

        public void DeleteMetalBonus()
        {
            metalBonusCount--;
            if (metalBonusCount < 0)
                metalBonusCount = 0;
            Text text = metalBonusLabelRef.GetComponent<Text>();
            text.text = metalBonusCount.ToString();
        }

        public void PauseGame()
        {
            pauseTimeScale = Time.timeScale;
            Time.timeScale = .0f;
            gamePaused = true;
        }

        public bool IsKilled()
        {
            return heroKilled;
        }

        public void KillHero()
        {
            if (!IsKilled())
            {
                soundLevelRef.Stop();
                gamePaused = true;
                clockBonusCount += 5;
                heroKilled = true;

                HeroController script = heroTransformRef.GetComponent<HeroController>();
                if (script)
                    script.Kill();
                
                Invoke("EndGameSound", 1f);
            }
        }

        public bool HaveClockBonus()
        {
            return clockBonusCount > 0;
        }

        public bool HaveMetalBonus()
        {
            return metalBonusCount > 0;
        }

        public void EndGameSound()
        {
            PlaySoundEffect(soundEndRef);
            Invoke("EndGame", 1.2f);
        }

        public void EndGame()
        {
            endGameTime = Time.timeSinceLevelLoad;
            AddRecord(score, GetLevelTimeAsString());

            Application.LoadLevel("End");
            gamePaused = false;
        }

        public void StartGame(bool resetParams = false)
        {
            if (gamePaused)
            {
                pauseGameTime += (Time.timeScale - pauseTimeScale);

                Time.timeScale = pauseTimeScale;
                pauseTimeScale = 0;

                if (resetParams)
                    ResetGameParams();
            }

            gamePaused = false;
            startGameTime = Time.timeSinceLevelLoad;
        }

        public float GetLevelTime(bool gameStarted = false)
        {
            return (gameStarted ? Time.timeSinceLevelLoad : endGameTime) - startGameTime - pauseGameTime;
        }

        public string GetLevelTimeAsString(bool gameStarted = false)
        {
            float time = GetLevelTime(gameStarted);
            string res = String.Format("{0:00}:{1:00}",
                (int)(time / 60), (int)(time % 60));

            return res;
        }

        public void ShowMainMenu(bool playSound = false)
        {
            if (playSound)
            {
                soundLevelRef.Stop();

                if (options.MusicOn == "Yes" && !soundMenuRef.isPlaying)
                    soundMenuRef.Play();
                else if (options.MusicOn == "No" && soundMenuRef.isPlaying)
                    soundMenuRef.Stop();
            }

            Application.LoadLevel("MainMenu");
        }

        public bool ReadGameParams()
        {
            bool res = false;

            try
            {
                if (!PlayerPrefs.HasKey("options"))
                {
                    TextAsset jsonFile = Resources.Load("options", typeof(TextAsset)) as TextAsset;
                    PlayerPrefs.SetString("options", jsonFile.text);
                    PlayerPrefs.Save();
                }

                string data = PlayerPrefs.GetString("options");
                var optionsReader = new JsonReader();
                options = optionsReader.Read<Options>(data);

                if (options.Version == null || options.Version != gameVersion)
                {
                    TextAsset jsonFile = Resources.Load("options", typeof(TextAsset)) as TextAsset;
                    PlayerPrefs.SetString("options", jsonFile.text);
                    PlayerPrefs.Save();

                    data = PlayerPrefs.GetString("options");
                    var optionsReaderAgain = new JsonReader();
                    options = optionsReaderAgain.Read<Options>(data);
                }

                if (!PlayerPrefs.HasKey("records"))
                {
                    TextAsset jsonFile = Resources.Load("records", typeof(TextAsset)) as TextAsset;
                    PlayerPrefs.SetString("records", jsonFile.text);
                    PlayerPrefs.Save();
                }

                data = PlayerPrefs.GetString("records");
                var recordsReader = new JsonReader();
                records = recordsReader.Read<Records>(data);

                if (records.Version == null || records.Version != gameVersion)
                {
                    TextAsset jsonFile = Resources.Load("records", typeof(TextAsset)) as TextAsset;
                    PlayerPrefs.SetString("records", jsonFile.text);
                    PlayerPrefs.Save();

                    data = PlayerPrefs.GetString("records");
                    var recordsReaderAgain = new JsonReader();
                    records = recordsReaderAgain.Read<Records>(data);
                }

                res = true;
            }

            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        public bool SaveGameParams()
        {
            bool res = false;

            try
            {
                var optionsWriter = new JsonWriter();
                string data = optionsWriter.Write(options);
                PlayerPrefs.SetString("options", data);
                PlayerPrefs.Save();
                res = true;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        public bool SaveRecords()
        {
            bool res = false;

            try
            {
                records.Items.Sort();

                int newSize = (records.Items.Count > 5) ? 5 : records.Items.Count;
                records.Items.RemoveRange(newSize, records.Items.Count - newSize);

                var recordsWriter = new JsonWriter();
                string data = recordsWriter.Write(records);
                PlayerPrefs.SetString("records", data);
                PlayerPrefs.Save();

                res = true;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return res;
        }

        public void AddRecord(int value, string levelTime)
        {
            records.Items.Add(new RecordItem(DateTime.Now, value, levelTime));
            if (value > records.MaxRecord)
                records.MaxRecord = value;

            SaveRecords();
        }

        public bool ReadLanguageFile(string lang)
        {
            bool res = false;
            Language langValue = Language.English;

            try
            {
                langValue = (Language)Enum.Parse(typeof(Language), lang);
            }
            catch (ArgumentException e)
            {
                options.Language = Language.English.ToString();
                Debug.LogException(e);
            }

            LanguageManager.LoadLanguageFile(langValue);
            res = true;

            return res;
        }

        public void PlaySoundEffect(AudioClip clip)
        {
            if (options.SoundOn == "Yes")
                audio.PlayOneShot(clip);
        }
        
        void Awake()
        {
            Init();
        }

        public void Init()
        {
            DontDestroyOnLoad(this);

            if (instance == null)
                instance = this;
            else if (instance != this)
                DestroyObject(gameObject);

            if (!ReadGameParams())
                Debug.LogError("Can't read options from file");

            if (ReadLanguageFile(options.Language))
                verticalSpeedDefault = verticalSpeed;
        }

        public void StartLevelHelp()
        {
            if (!levelHelpStarted)
            {
                Rigidbody2D tmp = heroTransformRef.gameObject.GetComponent<Rigidbody2D>();
                tmp.isKinematic = true;
                verticalSpeed = 0f;
                helpRef.SetBool("ShowHelp", true);
                levelHelpStarted = true;
                needLevelHelp = false;
            }
        }

        public void EndLevelHelp()
        {
            helpRef.SetBool("ShowHelp", false);
            verticalSpeed = startVerticalSpeed;
            Rigidbody2D tmp = heroTransformRef.gameObject.GetComponent<Rigidbody2D>();
            tmp.isKinematic = false;
        }

        void OnDestroy()
        {
            SaveRecords();

            if (panels.Count != 0)
                panels.RemoveRange(0, panels.Count);
            if (availableIndexs.Count != 0)
                availableIndexs.RemoveRange(0, availableIndexs.Count);
        }
    }
}

