﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class DebugInfo : MonoBehaviourEx {

    public bool debugShowInfoEnable = false;
    public bool debugGotoObjectEnable = false;
    public bool debugMobileEnable = false;
    public Transform debugGotoObjectRef = null;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// Вывод отладочной информации на экран. Использует за основу скрипт HUDFPS
    /// </summary>

    public Rect startRect = new Rect(10, 10, 110, 110); // The rect the window is initially displayed at.
    public bool updateColor = true; // Do you want the color to change if the FPS gets low
    public bool allowDrag = true; // Do you want to allow the dragging of the FPS window
    public float frequency = 0.5F; // The update frequency of the fps
    public int nbDecimal = 1; // How many decimal do you want to display

    private float accum = 0f; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private Color color = Color.white; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )
    private string sFPS = ""; // The fps formatted into a string.
    private GUIStyle style; // The style the text will be displayed at, based en defaultSkin.label.

    /// <summary>
    /// Проверка запущена ли игра на мобильном устройстве (или в имитации)
    /// </summary>
    /// <returns>true - мобильное устройство</returns>
    public bool IsMobile()
    {
        return (Application.platform == RuntimePlatform.Android || debugMobileEnable);
    }

    void Start()
    {
        if (IsMobile()) // для андройда отключаем vSync  - будет работать быстрее
        {
            string[] names = QualitySettings.names;
            for (int i = 0; i < names.Length; i++)
            {
                if (names[i] == "Simple")
                {
                    QualitySettings.SetQualityLevel(i, true);
                    break;
                }
            }

            //Application.targetFrameRate = 30; // должен быть отключен VSync Count в (или см. выше) Project Settings\Quality! (-1 - выключение настройки)

            QualitySettings.vSyncCount = 0;
        }


        StartCoroutine(FPS());
    }

    void Update()
    {
        if (debugShowInfoEnable)
        {
            accum += Time.timeScale / Time.deltaTime;
            ++frames;
        }
    }

    IEnumerator FPS()
    {
        // Infinite loop executed every "frenquency" secondes.
        while (true)
        {
            // Update the FPS
            float fps = accum / frames;
            sFPS = fps.ToString("f" + Mathf.Clamp(nbDecimal, 0, 10));

            //Update the color
            color = (fps >= 30) ? Color.green : ((fps > 10) ? Color.red : Color.yellow);

            accum = 0.0F;
            frames = 0;

            yield return new WaitForSeconds(frequency);
        }
    }

    void OnGUI()
    {
        if (debugShowInfoEnable)
        {
            // Copy the default label skin, change the color and the alignement
            if (style == null)
            {
                style = new GUIStyle(GUI.skin.label);
                style.normal.textColor = Color.white;
                style.alignment = TextAnchor.MiddleCenter;
            }

            GUI.color = updateColor ? color : Color.white;
            startRect = GUI.Window(0, startRect, DoMyWindow, "");
        }
    }

    void DoMyWindow(int windowID)
    {
        GUI.Label(new Rect(0, 0, startRect.width, startRect.height),
            sFPS + " FPS, " + QualitySettings.vSyncCount + " vSync\n" +
            Screen.width + "x" + Screen.height + "\n" +
            Screen.currentResolution.width + "x" + Screen.currentResolution.height + "\n" +
            Application.platform + "\n" +
            QualitySettings.names[QualitySettings.GetQualityLevel()] + "\n" +
            "Speed: " + GM.verticalSpeed + "\n" +
            "Score: " + GM.score,
            style);

        if (allowDrag) GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));
    }
}
