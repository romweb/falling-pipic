﻿using UnityEngine;
using System.Collections;
using GameGlobal;

/// <summary>
/// Плавное затемнение экрана
/// </summary>
[RequireComponent(typeof(Renderer))]
public class Blackout : MonoBehaviourEx {

    /// <summary>
    /// Коэффицент максимальной непрозрачности (1 - полностью не прозрачный)
    /// </summary>
    public float factorAlphaColor = 0f;
    /// <summary>
    /// Шаг изменения прозрачности
    /// </summary>
    public float stepAlphaColor = .02f;

    public Transform childBackground = null;

    private bool isCreated = false;
    private bool ChangeBackgroundNow = true;

    public Texture[] backgrounds = null;
    public float changeBackgroundInterval = 3f;
    int bgIndex = 0;

    void Start()
    {
        StartCoroutine(BackgroundCoroutine());
    }

    void OnDestroy()
    {
        StopCoroutine(BackgroundCoroutine());
        StopCoroutine(HideBackgroundCoroutine());
    }

    /// <summary>
    /// Запуск затемнения или осветления экрана
    /// </summary>
    public void StartBlackout()
    {
        if (!isCreated) // обеспечиваем запуск только одного корутина
        {
            isCreated = true;
            ChangeBackgroundNow = true;

            StartCoroutine(HideBackgroundCoroutine());
        }
    }
    
    IEnumerator HideBackgroundCoroutine()
    {
        while (true)
        {
            Color tempColor = myRenderer.material.GetColor("_Color");

            if (ChangeBackgroundNow)
            {
                childBackground.renderer.material.mainTexture = backgrounds[bgIndex];

                bgIndex++;
                if (bgIndex == backgrounds.Length)
                    bgIndex = 0;
                myRenderer.material.mainTexture = backgrounds[bgIndex];

                tempColor = myRenderer.material.GetColor("_Color");
                tempColor.a = factorAlphaColor;
                myRenderer.material.SetColor("_Color", tempColor);

                ChangeBackgroundNow = false;
            }

            while (tempColor.a <= 1f)
            {
                yield return new WaitForEndOfFrame();

                tempColor.a = tempColor.a + stepAlphaColor;
                myRenderer.material.SetColor("_Color", tempColor);
            }

            isCreated = false;
            yield return null;
        }
    }

    IEnumerator BackgroundCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(changeBackgroundInterval);

            StartBlackout();
        }
    }
}
