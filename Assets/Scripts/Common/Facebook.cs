﻿using UnityEngine;
using System;
using System.Collections;
using GameGlobal;

public class Facebook : MonoBehaviourEx
{
    /// <summary>
    /// ID приложения на facebook
    /// </summary>
    public string applicationId = null;
    /// <summary>
    /// Адрес, который будет добавлен в твит
    /// </summary>
    public string url = null;
    /// <summary>
    /// Адрес картинки
    /// </summary>
    public string pictureUrl = null;
    /// <summary>
    /// Заголовок сообщения
    /// </summary>
    public string messageCaption = null;
    /// <summary>
    /// Имя сообщения
    /// </summary>
    public string messageName = null;

    const string ShareUrl = "http://www.facebook.com/dialog/feed";

    public void Share()
    {
        string tmp = LanguageManager.GetText("social");

        Application.OpenURL(ShareUrl +
            "?app_id=" + WWW.EscapeURL(applicationId) +
            "&link=" + WWW.EscapeURL(url) +
            "&picture=" + WWW.EscapeURL(pictureUrl) +
            "&name=" + WWW.EscapeURL(messageName) +
            "&caption=" + WWW.EscapeURL(messageCaption) +
            "&description=" + WWW.EscapeURL(String.Format(tmp, GM.records.MaxRecord)) +
            "&redirect_uri=" + WWW.EscapeURL("http://facebook.com"));
    }
}
