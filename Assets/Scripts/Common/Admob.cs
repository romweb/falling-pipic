﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using GameGlobal;

public class Admob
{
    private BannerView bannerView;
    private InterstitialAd interstitial;

    //private bool bannerLoaded = false;
    private bool bannerShown = false;
    //private bool bannerRequest = false;

    public void RequestBanner(bool top = false)
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
            string adUnitId = "ca-app-pub-3940599218656149/5814221717";
#elif UNITY_IPHONE
            string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
            string adUnitId = "unexpected_platform";
#endif

        bannerView = new BannerView(
            adUnitId, AdSize.Banner, top ? AdPosition.Top : AdPosition.Bottom);
        bannerView.AdLoaded += HandleAdLoaded;
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);

        //bannerShown = bannerRequest = true;
     //   bannerView.Hide();
    }

//    public void RequestBanner()
//    {
//#if UNITY_EDITOR
//        string adUnitId = "unused";
//#elif UNITY_ANDROID
//            string adUnitId = "ca-app-pub-3940599218656149/5814221717";
//#elif UNITY_IPHONE
//            string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
//#else
//            string adUnitId = "unexpected_platform";
//#endif

//        // Create a 320x50 banner at the top of the screen.
//        bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
//        // Register for ad events.
//        bannerView.AdLoaded += HandleAdLoaded;
//        bannerView.AdFailedToLoad += HandleAdFailedToLoad;
//        bannerView.AdOpened += HandleAdOpened;
//        bannerView.AdClosing += HandleAdClosing;
//        bannerView.AdClosed += HandleAdClosed;
//        bannerView.AdLeftApplication += HandleAdLeftApplication;
//        // Load a banner ad.
//        bannerView.LoadAd(createAdRequest());
//    }


    public bool IsInterstitialLoaded()
    {
        return (interstitial != null) && interstitial.IsLoaded();
    }

    public void RequestInterstitial()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
            string adUnitId = "ca-app-pub-3940599218656149/7290954913";
#elif UNITY_IPHONE
            string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create an interstitial.
        interstitial = new InterstitialAd(adUnitId);
        if (interstitial != null)
        {
            //// Register for ad events.
            //interstitial.AdLoaded += HandleInterstitialLoaded;
            //interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
            //interstitial.AdOpened += HandleInterstitialOpened;
            //interstitial.AdClosing += HandleInterstitialClosing;
            interstitial.AdClosed += HandleInterstitialClosed;
            //interstitial.AdLeftApplication += HandleInterstitialLeftApplication;
            //// Load an interstitial ad.

            AdRequest request = new AdRequest.Builder().Build();
            interstitial.LoadAd(request);
        }
    }

    //// Returns an ad request with custom ad targeting.
    //private AdRequest createAdRequest()
    //{
    //    return new AdRequest.Builder() 
    //            .AddTestDevice(AdRequest.TestDeviceSimulator)
    //            .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
    //            .AddKeyword("game")
    //            .SetGender(Gender.Male)
    //            .SetBirthday(new DateTime(1985, 1, 1))
    //            .TagForChildDirectedTreatment(false)
    //            .AddExtra("color_bg", "9B30FF")
    //            .Build();

    //}

    public void ShowInterstitial()
    {
        if (interstitial != null)
        { 
            if (interstitial.IsLoaded())
            {
                interstitial.Show();
            }
        }
    }

    public void ShowBanner()
    {
        if (!bannerShown)
        {
            if (bannerView != null)
            {
                bannerView.Show();
                bannerShown = true;
            }
        }
    }

    public void HideBanner()
    {
        if (bannerShown)
        {
            if (bannerView != null)
            {
                bannerView.Hide();
              //  bannerView.Destroy();
               // bannerView = null;
            }

            bannerShown = false;
            //bannerLoaded = false;
            //bannerRequest = false;
        }
    }
    

    //#region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        if (bannerShown)
        {
            bannerView.Hide();
            bannerView.Destroy();
        }

        //HideBanner();
        //bannerLoaded = true;
    }

    //public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    //{
    //    //print("HandleFailedToReceiveAd event received with message: " + args.Message);
    //    //bannerLoaded = false;
    //}

    //public void HandleAdOpened(object sender, EventArgs args)
    //{
    //    //print("HandleAdOpened event received");
    //}

    //void HandleAdClosing(object sender, EventArgs args)
    //{
    //    //print("HandleAdClosing event received");
    //}

    //public void HandleAdClosed(object sender, EventArgs args)
    //{
    //    //print("HandleAdClosed event received");
    //}

    //public void HandleAdLeftApplication(object sender, EventArgs args)
    //{
    //    //print("HandleAdLeftApplication event received");
    //}

    //#endregion

    //#region Interstitial callback handlers

    //public void HandleInterstitialLoaded(object sender, EventArgs args)
    //{
    //    //print("HandleInterstitialLoaded event received.");
    //}

    //public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    //{
    //    //print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
    //}

    //public void HandleInterstitialOpened(object sender, EventArgs args)
    //{
    //    //print("HandleInterstitialOpened event received");
    //}

    //void HandleInterstitialClosing(object sender, EventArgs args)
    //{
    //    Application.Quit();
    //}

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        Application.Quit();
    }

    //public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    //{
    //    //print("HandleInterstitialLeftApplication event received");
    //}

    //#endregion
}
