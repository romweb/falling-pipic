﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class MetalBonusText : MonoBehaviourEx
{
    void Awake()
    {
        _Awake();
        GM.metalBonusLabelRef = gameObject.transform;
    }
}
