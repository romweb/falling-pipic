﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class AnimCheck : MonoBehaviourEx
{
    HeroController heroRef = null;
    int prevPanelId = 0;

    void Start()
    {
        heroRef = GM.heroTransformRef.GetComponent<HeroController>();
    }
	
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!heroRef.placed && coll.gameObject.CompareTag("Panel") && prevPanelId != coll.gameObject.GetInstanceID())
        {
            heroRef.placed = true;
            heroRef.needAnimation = true;
            prevPanelId = coll.gameObject.GetInstanceID();
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (heroRef.placed && coll.gameObject.CompareTag("Panel"))
        {
            heroRef.placed = false;
            heroRef.needAnimation = false;
        }
    }
}
