﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class MainMenu : MonoBehaviourEx
{
    void Start()
    {
        if (GM.options.MusicOn == "Yes" && !GM.soundMenuRef.isPlaying)
            GM.soundMenuRef.PlayDelayed(1f);

        if (GM.options.FirstStart == "Yes")
        {
            Application.LoadLevel("Languages");
        }

        AM.ShowBanner(GM.showAds);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GM.SaveGameParams();
            GM.SaveRecords();

            StopAllCoroutines();

            if (GM.showAds)
            {
                if (AM.IsInterstitialLoaded())
                    AM.ShowInterstitial();
                else
                    Application.Quit();
            }
            else
                Application.Quit();
        }
    }

    public void ExitButton()
    {
        if (GM.showAds)
        {
            if (AM.IsInterstitialLoaded())
                AM.ShowInterstitial();
            else
                Application.Quit();
        }
        else
            Application.Quit();
    }

    public void PlayButton()
    {
        if (GM.showAds)
            AM.ShowBanner(false);

        GM.StartGame(true);
        Application.LoadLevel("Level");
    }

    public void OptionsButton()
    {
        Application.LoadLevel("Options");
    }

    public void RecordsButton()
    {
        Application.LoadLevel("Records");
    }

    public void HelpButton()
    {
        Application.LoadLevel("Help");
    }
}
