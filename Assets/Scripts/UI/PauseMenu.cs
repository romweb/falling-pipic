﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class PauseMenu : MonoBehaviourEx
{
    public Transform buttonRef = null;
    bool needHideMenu = false;

    void Start()
    {
        if (buttonRef == null)
             Debug.LogError("PauseMenu.buttonRef isn't defined!");
    }

    void FixedUpdate()
    {
        if (needHideMenu)
        {
            myAnimator.SetBool("ShowMenu", false);
            myAnimator.SetBool("HideMenu", true);
            needHideMenu = false;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GM.SaveGameParams();
            GM.SaveRecords();

            StopAllCoroutines();

            if (GM.showAds)
            {
                if (AM.IsInterstitialLoaded())
                    AM.ShowInterstitial();
                else
                    Application.Quit();
            }
            else
                Application.Quit();
        }
    }
    
    public void PauseGame(int value)
    {
        if (value != 0)
            GM.PauseGame();
        else
            GM.StartGame();
    }

    public void ExitButton()
    {
        StopAllCoroutines();

        if (GM.showAds)
        {
            if (AM.IsInterstitialLoaded())
                AM.ShowInterstitial();
            else
                Application.Quit();
        }
        else
            Application.Quit();
    }

    public void MenuButton()
    {
        GM.ShowMainMenu(true);    
    }

    public void ContinueButton()
    {
        if (GM.showAds)
            AM.ShowBanner(false);

        if (buttonRef)
            buttonRef.gameObject.SetActive(true);
        needHideMenu = true;
        GM.StartGame();
    }
}
