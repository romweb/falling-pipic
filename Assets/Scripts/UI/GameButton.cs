﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class GameButton : MonoBehaviourEx
{
    public Transform menuRef = null;

    void Start()
    {
        if (menuRef == null)
            Debug.LogError("GameButton.menuRef isn't defined!");
    }

    public void MenuButton()
    {
        Animator anim = menuRef.GetComponent<Animator>();
        if (anim)
        {
            menuRef.gameObject.SetActive(true);
            gameObject.SetActive(false);
            anim.SetBool("HideMenu", false);
            anim.SetBool("ShowMenu", true);

            if (GM.showAds)
                AM.ShowBanner(true);
        }
    }
}
