﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class EndMenu : MonoBehaviourEx
{
    void Awake()
    {
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GM.SaveGameParams();
            GM.SaveRecords();

            StopAllCoroutines();

            if (GM.showAds)
            {
                if (AM.IsInterstitialLoaded())
                    AM.ShowInterstitial();
                else
                    Application.Quit();
            }
            else
                Application.Quit();
        }
    }

    void Start()
    {
        if (GM.options.MusicOn == "Yes")
        {
            GM.soundLevelRef.Stop();
            GM.soundMenuRef.Play();
        }

        AM.ShowBanner(GM.showAds);
    }

    public void ExitButton()
    {
        StopAllCoroutines();

        if (GM.showAds)
        {
            if (AM.IsInterstitialLoaded())
                AM.ShowInterstitial();
            else
                Application.Quit();
        }
        else
            Application.Quit();
    }

    public void PlayButton()
    {
        if (GM.showAds)
            AM.ShowBanner(false);

        GM.StartGame();
        Application.LoadLevel("Level");
    }

    public void MenuButton()
    {
        GM.ShowMainMenu(true);
    }
}
