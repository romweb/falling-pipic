﻿using UnityEngine;
using System.Collections;
using GameGlobal;
using UnityEngine.UI;

public class ScoreTextLostScene : MonoBehaviourEx
{
	void Start ()
    {
        Text text = gameObject.GetComponent<Text>();
        text.text = GM.score.ToString();
	}
}
