﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using GameGlobal;

public class ScoreTextLevel : MonoBehaviourEx
{
    private Text textRef = null;

    void Awake()
    {
        _Awake();
        if (GM)
            GM.scoreLevelTextRef = this;

        textRef = GetComponent<Text>();
    }

    public void StopAnimation()
    {
        myAnimator.SetBool("ShowText", false);
    }

    public void ShowText(BonusTextType type, string id, params object[] parameters)
    {
        string text = LanguageManager.GetText(id);
        if (!String.IsNullOrEmpty(text))
        {
            string textValue = null;
            if (parameters.Length == 0)
                textValue = text;
            else
            {
                try
                {
                    textValue = String.Format(text, parameters);
                }
                catch (FormatException e)
                {
                    Debug.LogException(e);
                }
            }

            if (!String.IsNullOrEmpty(textValue))
            {
                if (textRef)
                {
                    textRef.text = textValue;
                    myAnimator.SetBool("ShowText", true);
                    if (type == BonusTextType.Score)
                        Invoke("Score", 1f);
                }
            }
        }
    }

    void Score()
    {
        GM.ScoreUpdate();
    }
}
