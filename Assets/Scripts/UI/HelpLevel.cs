﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class HelpLevel : MonoBehaviourEx
{

    public void EndLevelHelp()
    {
        GM.EndLevelHelp();
    }

    public void Sound()
    {
        GM.PlaySoundEffect(GM.soundHelpRef);
    }

}
