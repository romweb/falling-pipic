﻿using UnityEngine;
using System.Collections;
using GameGlobal;
using UnityEngine.UI;

public class HighScoreTextLostScene : MonoBehaviourEx
{
    void Start()
    {
        Text text = gameObject.GetComponent<Text>();
        text.text = GameManager.instance.records.MaxRecord.ToString();
    }
}
