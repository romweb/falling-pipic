﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class RecordsMenu : MonoBehaviourEx
{
    public void MenuButton()
    {
        GM.ShowMainMenu();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GM.SaveGameParams();
            GM.SaveRecords();

            StopAllCoroutines();

            if (GM.showAds)
            {
                if (AM.IsInterstitialLoaded())
                    AM.ShowInterstitial();
                else
                    Application.Quit();
            }
            else
                Application.Quit();
        }
    }
}
