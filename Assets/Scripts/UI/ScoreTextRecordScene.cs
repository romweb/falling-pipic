﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameGlobal;
using UnityEngine.UI;

public class ScoreTextRecordScene : MonoBehaviourEx {

    public int index = 0;

    void Start()
    {
        try
        {
            RecordItem[] items = GM.records.Items.ToArray();
            if (index < items.Length)
            {
                Text text = gameObject.GetComponent<Text>();
                text.text = items[index].value.ToString();
                
                gameObject.SetActive(true);
            }
            else
                gameObject.SetActive(false);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }
}
