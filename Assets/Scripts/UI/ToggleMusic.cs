﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameGlobal;

public class ToggleMusic : MonoBehaviourEx
{
	void Start ()
    {
        Toggle toggle = gameObject.GetComponentInChildren<Toggle>();
        if (toggle)
            toggle.isOn = (GM.options.MusicOn == "Yes");
	}

    public void SetToggle()
    {
        Toggle toggle = gameObject.GetComponentInChildren<Toggle>();
        if (toggle)
            GM.options.MusicOn = toggle.isOn ? "Yes" : "No";
    }
}
