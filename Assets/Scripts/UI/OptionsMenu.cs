﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameGlobal;

public class OptionsMenu : MonoBehaviourEx
{
    void Start()
    {
        Toggle[] toggles = gameObject.GetComponentsInChildren<Toggle>();
        foreach (Toggle toggle in toggles)
        {
            Text label = toggle.GetComponentInChildren<Text>();
            if (label)
            {
                if (label.text == GM.options.Language)
                {
                    toggle.isOn = true;
                    break;
                }
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GM.SaveGameParams();
            GM.SaveRecords();

            StopAllCoroutines();

            if (GM.showAds)
            {
                if (AM.IsInterstitialLoaded())
                    AM.ShowInterstitial();
                else
                    Application.Quit();
            }
            else
                Application.Quit();
        }
    }

    public void MenuButton()
    {
        GM.SaveGameParams();
        GM.ShowMainMenu(true);
    }

    public void SetLanguage(string language)
    {
        if (language != GM.options.Language)
            GM.options.Language = language;
    }

    public void SetMusic()
    {
    }

    public void SetSound()
    {
    }
}
