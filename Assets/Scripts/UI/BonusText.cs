﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using GameGlobal;

public class BonusText : MonoBehaviourEx
{
    private Text textRef = null;
    private string textValue = null;

    void Awake()
    {
        _Awake();
        if (GM)
            GM.bonusLevelTextRef = this;

        textRef = GetComponent<Text>();
    }

    public void StopAnimation()
    {
        myAnimator.SetBool("ShowText", false);
    }

    public void ShowText(BonusTextType type, string id, params object[] parameters)
    {
        string text = LanguageManager.GetText(id);
        if (!String.IsNullOrEmpty(text))
        {
            if (parameters.Length == 0)
                textValue = text;
            else
            { 
                try
                {
                    textValue = String.Format(text, parameters);
                }
                catch (FormatException e)
                {
                    Debug.LogException(e);
                }
            }

            if (!String.IsNullOrEmpty(textValue))
            {
                if (textRef)
                {
                    textRef.text = textValue;

                    myAnimator.SetBool("ShowText", true);
                    if (type == BonusTextType.MetalBonus)
                        Invoke("MetalBonus", 1f);
                    else if (type == BonusTextType.ClockBonus)
                        Invoke("ClockBonus", 1f);
                }
            }
        }
    }

    void MetalBonus()
    {
        GM.metalBonusCount++;
        Text text = GM.metalBonusLabelRef.GetComponent<Text>();
        text.text = GM.metalBonusCount.ToString();
    }

    void ClockBonus()
    {
        GM.clockBonusCount++;
        Text text = GM.clockBonusLabelRef.GetComponent<Text>();
        text.text = GM.clockBonusCount.ToString();
    }

    void Score()
    {
        GM.ScoreUpdate();
    }
}
