﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GameGlobal;

public class ToggleSound : MonoBehaviourEx
{
	void Start ()
    {
        Toggle toggle = gameObject.GetComponentInChildren<Toggle>();
        if (toggle)
            toggle.isOn = (GM.options.SoundOn == "Yes");
	}
    public void SetToggle()
    {
        Toggle toggle = gameObject.GetComponentInChildren<Toggle>();
        if (toggle)
            GM.options.SoundOn = toggle.isOn ? "Yes" : "No";
    }
}
