﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class SelectLanguage : MonoBehaviourEx
{
    public void SetLanguage(string language)
    {
        if (language != GM.options.Language)
            GM.options.Language = language;

        GM.needLevelHelp = true;

        GM.options.FirstStart = "No";
        GM.SaveGameParams();

        Application.LoadLevel("MainMenu");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GM.SaveGameParams();
            GM.SaveRecords();

            StopAllCoroutines();

            if (GM.showAds)
            {
                if (AM.IsInterstitialLoaded())
                    AM.ShowInterstitial();
                else
                    Application.Quit();
            }
        }
    }
}
