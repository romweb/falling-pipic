﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameGlobal;
using UnityEngine.UI;

public class ScoreTextTime : MonoBehaviourEx
{
    void Start()
    {
        Text text = gameObject.GetComponent<Text>();
        text.text = GM.GetLevelTimeAsString();
    }        
}
