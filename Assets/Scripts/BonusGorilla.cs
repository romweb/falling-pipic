﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class BonusGorilla : MonoBehaviourEx
{
    public float verticalVelocity = -10f;
    public float colliderEnableAfterAttack = 0.2f;
    public AudioClip soundBitaRef = null;
    public AudioClip soundBrokenRef = null;
    public AudioClip soundGorillaRef = null;
    public AudioClip soundPainRef = null;
    private bool broken = false;

    void AnimationEnded()
    {
        myAnimator.SetBool("NeedAttack", false);
        myAnimator.SetBool("NeedAttackMetal", false);

        if (!GM.HaveMetalBonus())
        {
            if (GM.heroTransformRef)
            {
                GM.heroTransformRef.collider2D.isTrigger = true;
                GM.heroTransformRef.rigidbody2D.velocity = new Vector2(0f, verticalVelocity);
                GM.heroTransformRef.rigidbody2D.AddForce(GM.heroTransformRef.rigidbody2D.velocity);

                Invoke("SetTrigger", colliderEnableAfterAttack);
                Invoke("SoundPain", colliderEnableAfterAttack);

                PanelController panelScript = gameObject.GetComponentInParent<PanelController>();
                if (panelScript)
                    panelScript.SetInactivePanel();
            }
        }
    }

    void PlaySoundEffect(AudioClip clip)
    {
        if (GM.options.SoundOn == "Yes")
            myAudio.PlayOneShot(clip);
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Hero")
            PlaySoundEffect(soundGorillaRef);
    }

    void PlaySound()
    {
        if (soundBitaRef && soundBrokenRef)
        {
            if (GM.HaveMetalBonus())
                PlaySoundEffect(soundBrokenRef);
            else
                PlaySoundEffect(soundBitaRef);

            if (GM.heroTransformRef && !GM.IsKilled() && !GM.HaveMetalBonus())
            {
                Animator anim = GM.heroTransformRef.gameObject.GetComponent<Animator>();
                if (anim)
                    anim.SetBool("NeedGorilla", true);
            }
        }
    }

    void SoundPain()
    {
        if (soundPainRef)
            PlaySoundEffect(soundPainRef);
    }

    public void JumpHero()
    {
        if (!broken)
        {
            broken = true;
            PanelController parentScript = gameObject.GetComponentInParent<PanelController>();
            if (parentScript)
                parentScript.EnableAllTriggers(parentScript.gameObject, true);
        }
    }

    void SetTrigger()
    {
        if (GM.heroTransformRef)
            GM.heroTransformRef.collider2D.isTrigger = false;
    }
}
