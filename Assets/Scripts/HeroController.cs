﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class HeroController : MonoBehaviourEx {

    [HideInInspector]
    public bool needAnimation = false;
    [HideInInspector]
    public bool placed = false;

    public Sprite normalImage;
    public Sprite metalImage;

    public AudioClip soundClockRef = null;
    public AudioClip soundDelayRef = null;
    public AudioClip soundMetalRef = null;
    public AudioClip soundTrapRef = null;
    public AudioClip soundYahoooRef = null;
    public AudioClip soundExplosionRef = null;

    bool metalBallCoroutineStarted = false;
    bool clockCoroutineStarted = false;

    //float verticalSpeedTmp = 0f;

    public Transform startRef = null;
    public Collider2D topRegionRef = null;
    [Tooltip("Ссылка на кнопку паузы")]
    public Transform pauseButtonRef = null;
    [Tooltip("Ссылка на меню паузы")]
    public Transform pauseMenuRef = null;

    CameraController cameraScriptRef = null;

    public Animator helpRef = null;

    public bool HeroStarted { get; set; }

    void Awake()
    {
        _Awake();
        GM.heroTransformRef = gameObject.transform;
        GM.helpRef = helpRef;
        cameraScriptRef = thisCamera.GetComponent<CameraController>();
        HeroStarted = false;
    }

	void Start ()
    {
        GM.prevDistanceForScore = myTransform.position.y;
        myRigidbody2D.drag = GM.normalVerticalDrag;

        if (GM.options.MusicOn == "Yes")
        {
            GM.soundMenuRef.Stop();
            GM.soundLevelRef.PlayDelayed(1f);
        }

        Invoke("Yahooo", 0.4f);
        InvokeRepeating("Invoke_NeedBlinks", 5f, UnityEngine.Random.Range(3f, 8f));

        SetRigidbodyDrag();
	}

    void Invoke_NeedBlinks()
    {
        if (!GM.IsKilled() && !GM.HaveMetalBonus())
            myAnimator.SetBool("NeedBlinks", true);
    }
    
    void PlaySoundEffect(AudioClip clip)
    {
        if (GM.options.SoundOn == "Yes")
            myAudio.PlayOneShot(clip);
    }

    void Yahooo()
    {
        Invoke("Yahooo2", 0.8f);
        PlaySoundEffect(soundYahoooRef);
    }

    void Yahooo2()
    {
        myAnimator.SetBool("NeedYahoo", true);
    }

    void SetBottomTriggerOff()
    {
        topRegionRef.isTrigger = false;
    }

    void StopAnimation(int kill = 0)
    {
        needAnimation = false;
        if (kill == 1)
        {
            myAnimator.SetBool("NeedYahoo", false);
            myAnimator.SetBool("NeedGorilla", false);
            myAnimator.SetBool("NeedBlinks", false);
            myAnimator.SetBool("NeedKill", false);
            gameObject.SetActive(false);
        }
        else if (kill == 2)
        {
            myAnimator.SetBool("NeedYahoo", false);
            myAnimator.SetBool("NeedGorilla", false);
            myAnimator.SetBool("NeedBlinks", false);
        }
    }

    void FixedUpdate()
    {
        if (!GM.IsKilled())
        {
            myAnimator.SetBool("NeedSqueeze", needAnimation);
        }

        if (GM.HaveMetalBonus() && !metalBallCoroutineStarted)
            SetMetalBall(true);

        if (GM.HaveClockBonus() && !clockCoroutineStarted)
            SetClockBall(true);
    }
	
	void Update ()
    {
        float posChange = GM.prevDistanceForScore - myTransform.position.y;
        if (posChange >= GM.distanceForScore)
        {
            GM.prevDistanceForScore = myTransform.position.y;
            GM.AddScore(GM.scoreDistance);
        }
        if (GM.needLevelHelp && myTransform.position.y <= 0f)
        {
            GM.StartLevelHelp();
        }
	}

    void SetClockBall(bool value)
    {
        if (value)
        {
            //verticalSpeedTmp = GM.verticalSpeed;
            GM.verticalSpeed = GM.verticalSpeedPause;
            StartCoroutine(clock());
        }
        else
        {
            //GM.verticalSpeed = verticalSpeedTmp;
            StopCoroutine(clock());
        }
    }

    void SetMetalBall(bool value)
    {
        if (value)
        {
            myAnimator.SetBool("NeedBlinks", false);
            myAnimator.SetBool("IsMetal", true);
            mySpriteRenderer.sprite = metalImage;
            myRigidbody2D.drag = GM.metalVerticalDrag;
            StartCoroutine(metalBall());
        }
        else
        {
            myAnimator.SetBool("IsMetal", false);
            mySpriteRenderer.sprite = normalImage;
            myRigidbody2D.drag = GM.normalVerticalDrag;
            StopCoroutine(metalBall());
        }
    }

    void SetRigidbodyDrag(float value = 0f)
    {
        if (GM.HaveMetalBonus())
            myRigidbody2D.drag = GM.metalVerticalDrag + value;
        else
            myRigidbody2D.drag = GM.normalVerticalDrag + value;
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        switch (coll.gameObject.name)
        {
            case "PanelsRegion":
                Invoke("SetHeroStarted", 3f);
                break;
            case "TopSpeedZoneRegion":
            case "BottomSpeedZoneRegion1":
            case "BottomSpeedZoneRegion2":
                SetRigidbodyDrag();
                GM.hezoZone = HeroZone.NormalZone;
                GM.needAddBonusClockNow = false;
                myRigidbody2D.gravityScale = GM.normalGravityScale;
                break;
        }
    }

    void SetHeroStarted()
    {
        HeroStarted = true;
    }

    public void Kill()
    {
        pauseButtonRef.gameObject.SetActive(false);
        pauseMenuRef.gameObject.SetActive(false);
        myRigidbody2D.drag = 1000f;
        myAnimator.SetBool("NeedKill", true);
    }

    void PlayExplosion()
    {
        if (soundExplosionRef)
            PlaySoundEffect(soundExplosionRef);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        switch (coll.gameObject.name)
        {
            case "PanelsRegion":
                //if (HeroStarted)
                //    cameraScriptRef.FollowTheHero = true;
                break;
            case "BottomSpeedZoneRegion1":
                SetRigidbodyDrag(GM.verticalDragFactorSpeedZone1);
                GM.hezoZone = HeroZone.SpeepZone1;
                myRigidbody2D.gravityScale = GM.normalGravityScale;
                break;
            case "TopSpeedZoneRegion":
                SetRigidbodyDrag(GM.verticalDragFactorTopZone);
                GM.hezoZone = HeroZone.TopZone;
                myRigidbody2D.gravityScale = GM.topZoneGravityScale;
                break;
            case "BottomSpeedZoneRegion2":
                SetRigidbodyDrag(GM.verticalDragFactorSpeedZone2);
                GM.hezoZone = HeroZone.SpeepZone2;
                GM.needAddBonusClockNow = true;
                myRigidbody2D.gravityScale = GM.normalGravityScale;
                break;
            case "BottomRegion":
                GM.hezoZone = HeroZone.KillZone;
                GM.KillHero();
                break;
            case "TopRegion":
                Invoke("SetBottomTriggerOff", 0.2f);
                break;
            case "BonusMetalBall(Clone)":
                PlaySoundEffect(soundMetalRef);
                GM.AddMetalBonus();
                coll.gameObject.SetActive(false);
                SetMetalBall(true);
                break;
            case "BonusClock(Clone)":
                PlaySoundEffect(soundClockRef);
                GM.AddClockBonus();
                if (!GM.HaveClockBonus())
                    GM.verticalSpeedSave = GM.verticalSpeed;
                GM.verticalSpeed = GM.verticalSpeedPause;
                coll.gameObject.SetActive(false);
                break;
            case "BonusTrap(Clone)":
                {
                    BonusData data = coll.gameObject.GetComponent<BonusData>();
                    if (data && data.IsBonusActive())
                    {
                        PlaySoundEffect(soundTrapRef);
                        data.StartAnimation(!GM.HaveMetalBonus() ? "NeedClick" : "NeedClickMetal");
                        data.SetBonusActive(false);
                    }
                    break;
                }
            case "BonusGorilla(Clone)":
                {
                    BonusData data = coll.gameObject.GetComponent<BonusData>();
                    if (data && data.IsBonusActive())
                    {
                        data.StartAnimation(!GM.HaveMetalBonus() ? "NeedAttack" : "NeedAttackMetal");
                        data.SetBonusActive(false);
                        if (GM.HaveMetalBonus())
                            GM.AddScore(GM.scoreTrap);
                    }
                    break;
                }
            case "LeftRegion":
                myTransform.position = new Vector3(GM.recreateHeroPosX, startRef.transform.position.y, myTransform.position.z);
                myRigidbody2D.velocity = new Vector2(-GM.recreateHeroForce.x, GM.recreateHeroForce.y);
                myRigidbody2D.AddForce(myRigidbody2D.velocity);
                break;
            case "RightRegion":
                myTransform.position = new Vector3(-GM.recreateHeroPosX, startRef.transform.position.y, myTransform.position.z);
                myRigidbody2D.velocity = new Vector2(GM.recreateHeroForce.x, GM.recreateHeroForce.y);
                myRigidbody2D.AddForce(myRigidbody2D.velocity);
                break;
        }
    }

    IEnumerator clock()
    {
        while (GM.HaveClockBonus())
        {
            clockCoroutineStarted = true;
            yield return new WaitForSeconds(GM.clockInterval);
            GM.DeleteClockBonus();
        }

        if (clockCoroutineStarted)
            SetClockBall(false);
        clockCoroutineStarted = false;
        GM.verticalSpeed = GM.verticalSpeedSave;
    }

    IEnumerator metalBall()
    {
        while (GM.HaveMetalBonus())
        {
            metalBallCoroutineStarted = true;
            yield return new WaitForSeconds(GM.metalBallInterval);
            GM.DeleteMetalBonus();
        }

        if (metalBallCoroutineStarted)
            SetMetalBall(false);
        metalBallCoroutineStarted = false;
    }
}
