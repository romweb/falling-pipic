﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using GameGlobal;

public class CameraController : MonoBehaviourEx
{
    private Vector3 targetPos = Vector3.zero;
    private float speedIndex = 1.0f;

    public bool FollowTheHero { get; set;}
    private Vector3 followTheHeroPosition = Vector3.zero;

    void Awake()
    {
        _Awake();
        targetPos = myTransform.position;
    }

	void Start ()
    {
        StartCoroutine(cameraUpdate());
        StartCoroutine(ChangeSpeed());

        speedIndex = 1.0f;
	}

    public void ChangeVerticalTarget(float value)
    {
        targetPos.y += value;
    }

    void OnDestroy()
    {
        StopCoroutine(cameraUpdate());
        StopCoroutine(ChangeSpeed());
    }

    void Update()
    {
        if (FollowTheHero)
        {
            //if (followTheHeroPosition == Vector3.zero)
            //    followTheHeroPosition = transform.position;
            //else
            //{
            //    float dist = (followTheHeroPosition - GM.heroTransformRef.position).sqrMagnitude;
            //    if (dist >= 0f)
            //    { 
            //        FollowTheHero = false;
            //        followTheHeroPosition = Vector3.zero;
            //    }
            //}

            //targetPos = new Vector3(targetPos.x, GM.heroTransformRef.position.y, targetPos.z);

            targetPos += (Vector3.down * (GM.gamePaused ? 0.01f : GM.verticalSpeed) * Time.deltaTime * 0.8f);
        }
        else
            targetPos += Vector3.down * (GM.gamePaused ? 0.01f : GM.verticalSpeed) * Time.deltaTime;
    }

    IEnumerator ChangeSpeed()
    {
        while (true)
        {
            yield return new WaitForSeconds(GM.repeatTimeChangeSpeed);

            if (GM.HaveClockBonus())
                GM.verticalSpeed = GM.verticalSpeedPause;
            else
            {
                if (GM.verticalSpeed <= GM.maxVerticalSpeed)
                {
                    GM.verticalSpeed += Time.deltaTime * Mathf.Log(speedIndex * GM.verticalSpeedFactor);
                    speedIndex ++;
                }

                if (GM.verticalSpeed >= GM.speedBonusClock)
                    GM.needAddBonusClockNow = true;
            }

            GM.heroTransformRef.rigidbody2D.gravityScale = GM.verticalSpeed / 2; 
        }
    }

    /// <summary>
    /// Корутин плавного смещения камеры  
    /// </summary>
    /// <returns></returns>
    IEnumerator cameraUpdate()
    {
        while (true)
        {
            if (FollowTheHero)
                myTransform.position = Vector3.Lerp(myTransform.position, targetPos, Time.deltaTime * 1f);
            else 
                myTransform.position = Vector3.Lerp(myTransform.position, targetPos, Time.deltaTime * 1f);
            yield return 0;
        }
    }
}
