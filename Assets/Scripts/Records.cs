﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RecordItem : IComparable<RecordItem> 
{
    public RecordItem(DateTime time, int value, string levelTime)
    {
        this.time = time;
        this.value = value;
        this.levelTime = levelTime;
    }

    public DateTime time { get; set; }
    public int value { get; set; }
    public string levelTime { get; set; }

    public int CompareTo(RecordItem comparePart)
    {
        if (comparePart == null)
            return 1;
        else
            return comparePart.time.CompareTo(this.time);
    }
}

[System.Serializable]
public class Records
{
    public string Version;
    public int MaxRecord;
    public List<RecordItem> Items;
}
