﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class BonusData : MonoBehaviourEx
{
    public bool oneAnimationOnly = false;
    bool animationStarted = false;
    bool activeBonus = true;

    public void Reset()
    {
        ChangeAlpha(1f);

        animationStarted = false;
        SetBonusActive(true);
    }

    public bool IsBonusActive()
    {
        return activeBonus;
    }

    public void SetBonusActive(bool value)
    {
        activeBonus = value;
    }

    public void ChangeAlpha(float alpha)
    {
        Color newColor = mySpriteRenderer.color;
        newColor.a = alpha;
        mySpriteRenderer.color = newColor;
    }

    public void StartAnimation(string name)
    {
        if (IsBonusActive())
        {
            if ((oneAnimationOnly && !animationStarted) || !oneAnimationOnly)
            {
                myAnimator.SetBool(name, true);
                animationStarted = true;
                SetBonusActive(false);
            }
        }
    }

    public void EndAnimation(string name)
    {
        myAnimator.SetBool(name, false);
    }

    void Start()
    {
        Reset();
    }
}
