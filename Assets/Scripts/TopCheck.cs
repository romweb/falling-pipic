﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class TopCheck : MonoBehaviourEx
{
    public CameraController cameraRef = null;

    void Start()
    {
        if (cameraRef == null)
             Debug.LogError("TopCheck.cameraRef isn't defined!");
    }

    void OnTriggerEnter2D(Collider2D coll)
    { 
        if (coll.name == "TopSpeedZoneRegion")
        {
            if (cameraRef)
                cameraRef.FollowTheHero = true;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.name == "TopSpeedZoneRegion")
        {
            if (cameraRef)
                cameraRef.FollowTheHero = false;
        }
    }
}

