﻿using UnityEngine;
using System.Collections;
using GameGlobal;

public class SkipHeroPanel : MonoBehaviourEx
{
    void OnTriggerEnter2D(Collider2D coll)
    {
        switch (coll.gameObject.name)
        {
            case "Hero":
                coll.collider2D.isTrigger = true;
                Invoke("TriggerOff", 0.1f);
                break;
        }
    }

    void TriggerOff()
    {
        GM.heroTransformRef.gameObject.collider2D.isTrigger = false;
    }
}
