﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LanguageText : MonoBehaviourEx 
{
    Text text;

	void Start ()
    {
        text = GetComponent<Text>();
        if (text != null)
            text.text = LanguageManager.GetText(text.text);
    }
}
